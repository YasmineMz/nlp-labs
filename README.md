# nlp-labs

* create your virtualenv

``` virtualenv -p python3 ENV```

* activate virtualenv

```source ENV/bin/activate```

* install requirements

```pip install -r requirements.txt```